/* This file is part of KLudo, a KDE Project
 * Copyright 2019-2020 Richard Molitor <gattschardo@googlemail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use std::iter::FromIterator;

type Index = i32;

#[derive(Debug, Clone, PartialEq)]
pub struct Board {
    players: Vec<Player>,
}

#[derive(Debug, Clone, PartialEq)]
struct Player {
    coins: Vec<Coin>,
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum Coin {
    Spawn,
    Board(Index),
    HomeStretch(Index),
    Home,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Color {
    Red,
    Green,
    Yellow,
    Blue,
}

#[derive(Debug, PartialEq)]
enum Move {
    Spawn,
    Advance(Index), // origin
    //StackAdvance(Index),
    HomeAdvance(Index),
    //StackHomeAdvance(Index),
}

#[derive(Debug, PartialEq)]
pub struct TaggedMove {
    coin: usize, /* 0 .. 3 */
    m: Move,
}

fn is_spawn(m: &Move) -> bool {
    match *m {
        Move::Spawn => true,
        _ => false,
    }
}

fn is_advance(m: &Move) -> bool {
    match *m {
        Move::Advance(_) => true,
        _ => false,
    }
}

fn is_home_advance(m: &Move) -> bool {
    match *m {
        Move::HomeAdvance(_) => true,
        _ => false,
    }
}

fn player_offset(c: Color) -> Index {
    match c {
        Color::Red => 0,
        Color::Green => 13,
        Color::Yellow => 26,
        Color::Blue => 39,
    }
}

fn player_exit(c: Color) -> Index {
    match c {
        Color::Red => 50,
        Color::Green => 11,
        Color::Yellow => 24,
        Color::Blue => 37,
    }
}

fn player(c: Color) -> usize {
    match c {
        Color::Red => 0,
        Color::Green => 1,
        Color::Yellow => 2,
        Color::Blue => 3,
    }
}

fn safe(i: Index) -> bool {
    match i {
        0 | 8 | 13 | 21 | 26 | 34 | 39 | 48 => true,
        _ => false,
    }
}

fn color(i: usize) -> Color {
    match i {
        0 => Color::Red,
        1 => Color::Green,
        2 => Color::Yellow,
        3 => Color::Blue,
        _ => panic!("at the disco"),
    }
}

impl Default for Player {
    fn default() -> Self {
        Player {
            coins: [Coin::Spawn, Coin::Spawn, Coin::Spawn, Coin::Spawn].to_vec(),
        }
    }
}

impl Default for Board {
    fn default() -> Self {
        Board {
            players: Vec::from_iter(std::iter::repeat(Player::default()).take(4)),
        }
    }
}

fn get_player(b: &Board, c: Color) -> &Player {
    &b.players[player(c)]
}

fn player_won(p: &Player) -> bool {
    p.coins.iter().all(|c| *c == Coin::Home)
}

pub fn won(b: &Board, p: Color) -> bool {
    player_won(get_player(b, p))
}

fn occupied(b: &Board, i: Index) -> Option<Color> {
    for (c, p) in b.players.iter().enumerate() {
        for coin in &p.coins {
            match coin {
                Coin::Board(j) => {
                    if *j == i {
                        return Some(color(c));
                    }
                }
                _ => (),
            }
        }
    }
    None
}

fn place(b: &Board, i: Index) -> (Coin, Option<Color>) {
    let r = Coin::Board(i);
    if safe(i) {
        (r, None)
    } else {
        match occupied(b, i) {
            None => (r, None),
            Some(p) => (r, Some(p)),
        }
    }
}

fn eat_index(coins: &Vec<Coin>, coin: &Coin) -> Option<usize> {
    for (i, c) in coins.iter().enumerate() {
        if c == coin {
            return Some(i);
        }
    }
    None
}

fn advance_home(r: Index) -> Option<(Coin, Option<Color>)> {
    if r == 6 {
        Some((Coin::Home, None))
    } else if r < 6 {
        Some((Coin::HomeStretch(r), None))
    } else {
        None
    }
}

fn try_advance_home(m: Move, rest: Index) -> Option<Move> {
    if rest > 6 {
        None
    } else {
        Some(m)
    }
}

fn guarded_pure<T>(v: T, b: bool) -> Option<T> {
    if b {
        Some(v)
    } else {
        None
    }
}

fn eligible_coin(b: &Board, c: Color, m: &TaggedMove) -> Option<Coin> {
    let p = get_player(b, c);
    let coin = p.coins[m.coin].clone();
    let mv = &m.m;
    match coin {
        Coin::Spawn => guarded_pure(coin, is_spawn(mv)),
        Coin::Board(_) => guarded_pure(coin, is_advance(mv)),
        Coin::HomeStretch(_) => guarded_pure(coin, is_home_advance(mv)),
        _ => None,
    }
}

fn enter_home(p: Color, cur: Index, roll: Index) -> Result<Index, Index> {
    const BOARD_SIZE: Index = 52;
    let x = player_exit(p);
    let j = cur + roll;
    if cur <= x && j > x {
        Ok(j - x)
    } else {
        Err(j % BOARD_SIZE)
    }
}

fn lowlevel_advance(
    b: &Board,
    p: Color,
    m: &TaggedMove,
    roll: Index,
) -> Option<(Coin, Option<Color>)> {
    match eligible_coin(b, p, m) {
        None => None,
        Some(Coin::Spawn) => {
            if roll == 6 {
                Some(place(b, player_offset(p)))
            } else {
                None
            }
        }
        Some(Coin::Board(i)) => match enter_home(p, i, roll) {
            Ok(h) => advance_home(h),
            Err(j) => Some(place(b, j)),
        },
        Some(Coin::HomeStretch(i)) => advance_home(i + roll),
        Some(Coin::Home) => None,
    }
}

fn try_advance(b: &Board, p: Color, c: &Coin, roll: Index) -> Option<Move> {
    match c {
        Coin::Spawn => {
            if roll == 6 {
                match occupied(b, player_offset(p)) {
                    None => Some(Move::Spawn),
                    _ => None,
                }
            } else {
                None
            }
        }
        Coin::Board(i) => match enter_home(p, *i, roll) {
            Ok(h) => try_advance_home(Move::Advance(*i), h),
            Err(j) => match occupied(b, j) {
                None => Some(Move::Advance(*i)),
                _ => None,
            },
        },
        Coin::HomeStretch(i) => try_advance_home(Move::HomeAdvance(*i), i + roll),
        Coin::Home => None,
    }
}

pub fn advance(b: &Board, c: Color, m: &TaggedMove, roll: Index) -> Board {
    match lowlevel_advance(b, c, m, roll) {
        None => panic!("exclamation point"),
        Some((coin, maybe_eaten)) => {
            let mut ps = b.players.clone();

            // update eaten coin (if any)
            match maybe_eaten {
                None => (),
                Some(victim_color) => {
                    let victim = &mut ps[player(victim_color)];
                    match eat_index(&victim.coins, &coin) {
                        None => panic!("no food"),
                        Some(i) => victim.coins[i] = coin.clone(),
                    }
                }
            }

            // update players coin
            let p = &mut ps[player(c)];
            p.coins[m.coin] = coin;

            Board { players: ps }
        }
    }
    // maybe return winner
}

pub fn possible_moves(b: &Board, c: Color, roll: Index) -> Vec<TaggedMove> {
    let p = get_player(b, c);
    let mut r = vec![];
    for (i, coin) in p.coins.iter().enumerate() {
        match try_advance(&b, c, coin, roll) {
            None => (),
            Some(m) => r.push(TaggedMove { coin: i, m: m }),
        }
    }
    r
}

fn roll() -> Index {
    4
}

fn choose_move(moves: &Vec<TaggedMove>) -> &TaggedMove {
    &moves[0]
}

pub fn winner() -> Color {
    let mut b = Board::default(); // X
    let playing = vec![Color::Red, Color::Green, Color::Yellow, Color::Blue];
    loop {
        for c in &playing {
            let d = roll(); // TODO re-roll on 6
            let moves = possible_moves(&b, *c, d); // X
            if moves.len() > 0 {
                let m = choose_move(&moves);
                b = advance(&b, *c, m, d); // X
                if won(&b, *c) {
                    return *c;
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{
        advance, color, is_advance, is_home_advance, is_spawn, player, player_exit, player_offset,
        player_won, possible_moves, won, Board, Coin, Color, Index, Move, Player, TaggedMove,
    };

    fn assert_fresh_player(p: &Player) {
        assert_eq!(p.coins.len(), 4);
        assert_eq!(p.coins.iter().all(|c| *c == Coin::Spawn), true);
    }

    #[test]
    fn test_init_player() {
        assert_fresh_player(&Player::default());
    }

    #[test]
    fn test_init() {
        let b = Board::default();
        assert_eq!(b.players.len() <= 4, true);
        assert_eq!(b.players.len() >= 2, true);

        for p in b.players {
            assert_fresh_player(&p);
        }
    }

    fn all_colors() -> Vec<Color> {
        vec![Color::Red, Color::Green, Color::Yellow, Color::Blue]
    }

    #[test]
    fn test_no_winner_on_initial_board() {
        for c in all_colors() {
            assert_eq!(won(&Board::default(), c), false);
        }
    }

    fn cold_rolls() -> Vec<Index> {
        vec![1, 2, 3, 4, 5]
    }

    fn hot_rolls() -> Vec<Index> {
        vec![6]
    }

    fn all_coins() -> Vec<usize> {
        vec![0, 1, 2, 3]
    }

    fn spawn_coin(coin: usize) -> TaggedMove {
        TaggedMove {
            coin,
            m: Move::Spawn,
        }
    }

    fn all_spawns() -> Vec<TaggedMove> {
        all_colors()
            .into_iter()
            .map(|c| player(c))
            .map(|i| spawn_coin(i))
            .collect()
    }

    #[test]
    fn test_need_hot_roll_on_initial_board() {
        let b = Board::default();
        for c in all_colors() {
            for r in cold_rolls() {
                assert_eq!(possible_moves(&b, c, r), vec![]);
            }

            for r in hot_rolls() {
                assert_eq!(possible_moves(&b, c, r), all_spawns());
            }
        }
    }

    fn advance_coin(coin: usize, p: Index) -> TaggedMove {
        TaggedMove {
            coin,
            m: Move::Advance(p),
        }
    }

    #[test]
    fn test_spawn_then_advance() {
        for c in all_colors() {
            for coin in all_coins() {
                let b = advance(&Board::default(), c, &spawn_coin(coin), hot_rolls()[0]);
                for r in cold_rolls() {
                    let moves = possible_moves(&b, c, r);
                    assert_eq!(moves, vec![advance_coin(coin, player_offset(c))]);
                    assert_ne!(b, advance(&b, c, &moves[0], r));
                }
            }
        }
    }

    #[test]
    fn test_is_spawn() {
        assert_eq!(is_spawn(&Move::Spawn), true);
        for ref m in vec![
            Move::Advance(0),
            //Move::StackAdvance(0),
            Move::HomeAdvance(0),
            //Move::StackHomeAdvance(0),
        ] {
            assert_eq!(is_spawn(m), false);
        }
    }

    #[test]
    fn test_is_advance() {
        assert_eq!(is_advance(&Move::Advance(0)), true);
        for ref m in vec![
            Move::Spawn,
            //Move::StackAdvance(0),
            Move::HomeAdvance(0),
            //Move::StackHomeAdvance(0),
        ] {
            assert_eq!(is_advance(m), false);
        }
    }

    #[test]
    fn test_is_home_advance() {
        assert_eq!(is_home_advance(&Move::HomeAdvance(0)), true);
        for ref m in vec![
            Move::Spawn,
            Move::Advance(0),
            //Move::StackAdvance(0),
            //Move::StackHomeAdvance(0),
        ] {
            assert_eq!(is_home_advance(m), false);
        }
    }

    fn push_coin(p: &Player, c: Coin) -> Player {
        let mut coins = p.coins.clone();
        coins.push(c);
        Player { coins }
    }

    #[test]
    fn test_player_won() {
        let p = Player { coins: vec![] };
        assert_eq!(player_won(&p), true);

        let home = Coin::Home;
        let home1 = push_coin(&p, home);
        assert_eq!(player_won(&home1), true);
        assert_eq!(player_won(&push_coin(&home1, home)), true);

        let outside = vec![Coin::Spawn, Coin::Board(0), Coin::HomeStretch(0)];
        for c in outside {
            assert_eq!(player_won(&push_coin(&p, c)), false);
            assert_eq!(player_won(&push_coin(&home1, c)), false);
        }
    }

    #[test]
    fn test_player_color() {
        for c in all_colors() {
            assert_eq!(c, color(player(c)));
        }
    }

    #[test]
    fn test_single_step_to_home() {
        for c in all_colors() {
            for coin in all_coins() {
                let mut b = advance(&Board::default(), c, &spawn_coin(coin), hot_rolls()[0]);
                let mut steps_to_home = 50;
                while steps_to_home > 0 {
                    let roll = 1;
                    let moves = possible_moves(&b, c, roll);
                    assert_eq!(moves.len(), 1);
                    assert!(is_advance(&moves[0].m));
                    b = advance(&b, c, &moves[0], roll);
                    steps_to_home -= 1;
                }

                for home_entry_roll in cold_rolls() {
                    let moves = possible_moves(&b, c, home_entry_roll);
                    assert_eq!(
                        moves,
                        vec![TaggedMove {
                            coin,
                            m: Move::Advance(player_exit(c))
                        }]
                    );
                    let home_board = advance(&b, c, &moves[0], home_entry_roll);
                    match home_board.players[player(c)].coins[coin] {
                        Coin::HomeStretch(j) => assert_eq!(j, home_entry_roll),
                        _ => panic!("Coin should have entered home stretch"),
                    }
                }
            }
        }
    }
}
